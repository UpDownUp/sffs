# SFFS  

A simulated flash memory filesystem that uses a ``memory.hex`` file instead of physical flash memory.  
Provides user-accessible functions to manipulate flash memory in a realistic manner on ``memory.hex``, performing read/write operations on the page level and marking/preserving individual logs.  
sffs does NOT do wear leveling. 


## Build
Clone the project, then compile main.c. During development, SFFS was compiled with the following command:  

    gcc -std=c17 main.c -o sffs

You can then run it using  

    ./sffs
Which, without additional arguments, prints a summary of usage instructions.

## Behavior
Memory is made up of 256 pages, each page containing 32 logs, and each log is 8 bytes. 
The last page is reserved for metadata.  

The filesystem is meant to be written like a ring buffer. At each write or erase, the position of the last modification is saved, and the user can only write or erase from there. At the last non-reserved page, the cursor resets to start, but free space is marked as 0% until next format. The cursor only goes forward on write and backward on erase.  
For a ring buffer use case, __the cursor position can be considered equal to the number of occupied logs + 1__.  
The user can read from any position.


## Usage
At first use, run ``sffs f`` (format). This will create a ``memory.hex`` file (if not already existing), size it correctly and write metadata to the last (reserved) page.


### Command list:  

Usage: ``sffs [command] (option)``  
       [required parameter] (optional parameter)  
- Format -     Blanks all bits and writes  metadata information (Recommended at first use).  
   Usage:      ``sffs f``

- Erase -      Erases n (1 if not specified) pages, backwards starting from cursor position.  
   Usage:      ``sffs e (n)``

- Read -       Read n (1 if not specified) logs or pages forwards starting from given ID.  
   Usage:      ``sffs r [l|p] [id] (n)``

- Write -      Writes up to 0xFFFF bytes to flash at the cursor position. Interpreted as string.  
   Usage:      ``sffs w [data] ``

- Meta -        Read out metadata (file and user-accessible memory size, free space, cursor position)  
   Usage:      ``sffs m``

- Unit test -   Performs read/write test with the given data at the given log position  
   Usage:      ``sffs u (log position) [data]`` 

- Info -        Information about filesystem behavior  
   Usage:      ``sffs i``

## Extras
In ``main()``, there are several operations that were commented out that may be uncommented to use as tests.  
Most are identical to using arguments to do the same thing, but some can do things that should not normally be accessible to the user - Nuun for example writes a very large amount of data to test overflow.