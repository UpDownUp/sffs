#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <stdbool.h>

// -- Defines and stuff
#define LOG_SIZE                (8) // bytes
#define PAGE_SIZE_BYTES         (256)
#define PAGE_SIZE_LOGS          (PAGE_SIZE_BYTES / LOG_SIZE)
#define PAGE_COUNT              (256)
#define METADATA_PAGE_ID        (255)
#define METADATA_BYTE_OFFSET    (PAGE_SIZE_BYTES * METADATA_PAGE_ID) // Last page

#define METADATA_ID_VERHASH     (0x1FE0)
#define METADATA_ID_FILESIZE    (0x1FE1)
#define METADATA_ID_MEMSIZE     (0x1FE2)
#define METADATA_ID_FREESPACE   (0x1FE3)
#define METADATA_ID_CURSOR      (0x1FE4)
#define METADATA_ID_BLOCKMAP    (0x1FE5) // PAGE_COUNT-bit blockmap, showing whether each block is allocated
#define METADATA_ID_NEXT        (0x1FED)

// Single log struct
typedef struct {
    uint16_t id;
    uint8_t data[LOG_SIZE - 2];
} log_t; // Could also be handled as a uint8_t[LOG_SIZE]
// A page would be treated as a simple uint8_t[PAGE_SIZE_BYTES], or a log_t[PAGE_SIZE_LOGS].

typedef struct {
    uint16_t ver_hash[3];
    uint32_t filesize;
    uint32_t memsize;
    uint32_t freespace;
    uint32_t cursorpos; // log, not byte
    // At the last log, cursorpos on metadata will be set to 0x2000.
    // Past that point, subsequent writes will loop from log 0x00,
    // but bit 13 (0x2000) of cursorpos will not be reset.
    // This will indicate 100% used space. Ring buffer will still cycle normally.
} meta_t;

// -- Function declarations
// Address conversion
uint16_t byte_addr_to_log(uint16_t byte_addr);
uint8_t log_id_to_page(uint16_t log_id);
uint32_t page_addr_to_byte(uint8_t page_id);
uint8_t log_addr_in_page(uint16_t log_id);
uint16_t calc_formatted_size(uint16_t raw_size);

// Ultra-boilerplate
FILE * mount_at(uint32_t pos, bool write_access);
void unmount(FILE * file_ptr);

// -- Level 1 - depends only on ultra-boilerplate
// Read
uint8_t * read_page(uint8_t page_id);
uint32_t get_file_size(void);   // Whole file, including reserved by filesystem 
uint32_t get_memory_size(void); // Total memory accessible to user

// Erase
void erase_page(uint8_t page_id);
// -- Level 1.1 - write_page depends on erase_page,
// and also depends on update_meta (and vice versa), but its base functionality does not
void write_page(uint8_t page_id, uint8_t page_content[PAGE_SIZE_BYTES]);

// -- Level 2 - depends on level 1/1.1
uint8_t * read_log(uint16_t log_id);
uint8_t write_one_log(log_t log);
void erase_flash(void);
void update_meta(uint8_t page_id, uint8_t * page_content, bool is_write); // updates free space and cursor log position (must be given)

// -- Level 3 - any higher
uint32_t calc_free_space(void);  // Depends on read_log
void format_flash(void);        // Depends on get_free_space


// Not yet sorted
uint8_t * format_data(uint16_t log_id, uint8_t *raw_bytes, uint16_t n_bytes);
uint8_t * strip_data(uint8_t *log_bytes, uint16_t n_bytes);
void write_raw_data(uint16_t log_id, uint8_t *raw_data, uint16_t n_bytes);
uint8_t * read_n_logs(uint32_t log_id, uint16_t n);
meta_t read_meta(void);


// -- Function definitions
// Address conversion
// TODO you can get points here for making them macros instead for le ultra super embedded performance
uint16_t byte_addr_to_log(uint16_t byte_addr) {
    // Logs are sequential chunks of 8 bytes each.
    return (byte_addr / LOG_SIZE);
}

uint32_t log_id_to_byte(uint8_t log_id) {
    return log_id * LOG_SIZE;
}

// Logs are sequential with 32 (0x20) logs to a page.
uint8_t log_id_to_page(uint16_t log_id) {
    return (log_id / PAGE_SIZE_LOGS);
}

uint32_t page_addr_to_byte(uint8_t page_id) {
    return page_id * PAGE_SIZE_BYTES;
}

uint32_t first_log_in_page(uint8_t page_id) {
    return page_id * PAGE_SIZE_LOGS;
}

uint8_t log_addr_in_page(uint16_t log_id) { // translates log ID to its byte address within the page
    // There are 0x20 neatly packed logs per page, 8 bytes each. 
    return ((log_id % PAGE_SIZE_LOGS) * LOG_SIZE);
}

uint16_t calc_formatted_size(uint16_t raw_size) {
    return ((raw_size / (LOG_SIZE - sizeof(uint16_t))) * LOG_SIZE) + ((raw_size % (LOG_SIZE - sizeof(uint16_t)) != 0) * LOG_SIZE);
}


// Ultra-boilerplate
// Opens the file 
FILE * mount_at(uint32_t pos, bool write_access) {
    FILE* fp;
    uint8_t err = EXIT_FAILURE;
    uint8_t retry = 0;
    while (retry < 10 && err != EXIT_SUCCESS) {

        if (write_access) {
            fp = fopen("memory.hex", "rb+");
        } else {
            fp = fopen("memory.hex", "rb");
        }

        if (ferror(fp)) {
            perror("Failed to open memory.hex"); // why? how to fix?
            printf("Retry %d... \n", ++retry);
            
        } else {
            err = EXIT_SUCCESS;
        }
    }
    if (err != EXIT_SUCCESS) {
        perror("Failed to open memory.hex at requested position after 10 retries");
        return NULL;
    }

    uint8_t err_code = fseek(fp, pos, SEEK_SET);
    if (err_code) {
        perror("Failed to seek to position");
    }
    // printf("Sought to %d, Actual pos %ld \n", pos, ftell(fp));

    return fp;
    // Returned pointer must be unmounted by caller.
}

// Closes the file
void unmount(FILE * file_ptr) {
    int err_code = fclose(file_ptr);
    if (err_code != 0) {
        perror("Unmount failed");
    }
}

// --- API 

uint32_t get_file_size(void) {
    FILE* fp = mount_at(0, false);
    if (!fp) {
        printf("Unable to mount flash. \n");
        return 0;
    }
    fseek(fp, 0, SEEK_END);
    uint32_t size = ftell(fp);
    unmount(fp);
    return size;
}

uint32_t get_memory_size(void) {
    FILE* fp = mount_at(0, false);
    if (!fp) {
        printf("Unable to mount flash. \n");
        return 0;
    }
    fseek(fp, 0, SEEK_END);
    uint32_t size = (ftell(fp) - PAGE_SIZE_BYTES); // subtract metadata page
    unmount(fp);
    return size;
}

void update_meta(uint8_t page_id, uint8_t * page_content, bool is_write) {
    
    // Update free space and cursor position.
    if (page_id != METADATA_PAGE_ID) {
        // Get last used (non-null) log ID from current stream,
        // and put the cursor in front of it.
        // If log is improperly formatted, the cursor will be set
        // at the first log in the next page.

        log_t * as_logs = (log_t *) page_content;        
        uint32_t cursor_pos = (uint32_t) byte_addr_to_log(page_addr_to_byte(page_id));
        // after erase, cursor position is the beginning of the erased page.

        if (is_write) {
            for (uint8_t i = 0; i < PAGE_SIZE_LOGS; i++) {
                /*
                for (uint8_t ct = 0; ct < LOG_SIZE - sizeof(uint16_t); ct++) {
                    printf("0x%.2X ", (uint8_t) as_logs[i].data[ct] );
                }
                printf("id %.4X // cursor pos 0x%.4X \n", as_logs[i].id, cursor_pos);
                */

                if (as_logs[i].id > (uint16_t) cursor_pos) {
                    cursor_pos = (uint32_t) as_logs[i].id;
                }
            }
            cursor_pos++;   // Cursor position is *after* last write.

            // reset cursor to start at the end (flash is a ring buffer)
                // how to detect invalid logs?
            if (cursor_pos >= METADATA_BYTE_OFFSET) {
                cursor_pos = 0x00;
            }
        }
        /*
        printf("Cursor set: 0x%.4X (0x%.2X 0x%.2X) \n", cursor_pos, 
                                                          cursor_pos & 0xFF,
                                                          (cursor_pos >> 8) & 0xFF);
        */

        uint8_t * meta_page = read_page(METADATA_PAGE_ID);
    
        // Free space calculation depends on cursor position. // todo should depend on block map!
        memcpy(meta_page + (4 * LOG_SIZE), (uint8_t[]){METADATA_ID_CURSOR & 0xFF, 
                                                          ((METADATA_ID_CURSOR >> 8) & 0xFF),
                                                          0x00, 0x00,
                                                          cursor_pos & 0xFF,
                                                          (cursor_pos >> 8) & 0xFF,
                                                          (cursor_pos >> 16) & 0xFF,
                                                          (cursor_pos >> 24) & 0xFF,
                                                          0x00, 0x00},
                                                          sizeof(uint8_t) * LOG_SIZE ) ;
        
        // Log 0x1FE3: Free space (to user) - calculated directly from cursor pos
        uint32_t memsize = *((uint32_t *) (meta_page + log_addr_in_page(METADATA_ID_MEMSIZE) / sizeof(uint32_t)) + sizeof(uint32_t));
        uint32_t freespace = 0;
        // If cursor is set to 0x2000 once, freespace is marked to 0 until next format.
        if ((cursor_pos & 0x2000) == 0) {
            freespace = memsize - (cursor_pos * LOG_SIZE);
        }
        //printf("memsize: 0x%.8X, freespace = 0x%.8X pid: %.2X cursorpos: 0x%.8X used bytes: 0x%.8X \n",
        //                memsize,        freespace,          page_id,    cursor_pos,   ( cursor_pos * LOG_SIZE));
        
        memcpy(meta_page + (3 * LOG_SIZE), (uint8_t[]){METADATA_ID_FREESPACE & 0xFF, 
                                                          ((METADATA_ID_FREESPACE >> 8) & 0xFF),
                                                          0x00, 0x00,
                                                          freespace & 0xFF,
                                                          (freespace >> 8) & 0xFF,
                                                          (freespace >> 16) & 0xFF,
                                                          (freespace >> 24) & 0xFF},
                                                          sizeof(uint8_t) * LOG_SIZE) ;
        
        write_page(METADATA_PAGE_ID, meta_page);
        free(meta_page);
    }
}


uint8_t *read_page(uint8_t page_id) {
    FILE * fp = mount_at(page_addr_to_byte(page_id), false);
    if (!fp) {
        printf("Failed to mount!");
        return NULL; // todo Caller should handle
    }

    uint8_t * buf = (uint8_t *) calloc(PAGE_SIZE_BYTES, sizeof(uint8_t));
    
    uint16_t count = fread((void *) buf, sizeof(uint8_t), PAGE_SIZE_BYTES, fp);
    // Handle failure to count
    if (count != PAGE_SIZE_BYTES) {
        printf("Error: Read only %d instead of %d elements on page %.2X! \n", count, PAGE_SIZE_BYTES, page_id);
    }

    unmount(fp);
    return buf;
     // Returned pointer must be freed by caller.
}

uint8_t * read_log(uint16_t log_id) {
    //printf("Reading log 0x%.4X", log_id);
    uint8_t * wholepage = read_page(log_id_to_page(log_id));

    /*
     //print page
    for (uint16_t i = 0; i < PAGE_SIZE_BYTES; i++) {
        if (i % 8 == 0) printf("\n");
        printf("0x%.2X ", wholepage[i]);
    }
    printf("\n");
    */

    uint8_t * log = malloc(sizeof(log_t));
    memcpy(log, wholepage + log_addr_in_page(log_id), LOG_SIZE);
    free(wholepage);
    // printf("Log: 0x%.8X 0x%.8X\n", *(uint32_t *) log, *((uint32_t *) (log + 4)));
    return log;
    // Returned pointer must be freed by caller.
}

// Reads up to n logs starting from the current one.
// Returns a uint8_t[n * LOG_SIZE]. 
// Will not read past the end of user accesible memory.
uint8_t * read_n_logs(uint32_t log_id, uint16_t n) {
    /*
    n <= next_page_log - log_id         print n logs
    n >  next_page_log - log_id         print next_page_log - log_id logs, then go to next page
    */
    n = fmin(n, byte_addr_to_log(METADATA_BYTE_OFFSET) - log_id);
    uint8_t *logs = calloc(n * LOG_SIZE, sizeof(uint8_t));
    int16_t logs_done = 0; 
    while (logs_done < n) {
        uint16_t next_page_log = first_log_in_page(log_id_to_page(log_id) + 1);
        uint16_t copysize_logs = fmin(n - logs_done, (next_page_log - log_id) );
    //    printf("log_id: 0x%.8X, logs_done: %d, next_page_log: 0x%.4X, copysize_logs: 0x%.4X\n",
    //            log_id,         logs_done,     next_page_log,         copysize_logs);

        uint8_t * wholepage = read_page(log_id_to_page(log_id));
        if (wholepage == NULL) {
            printf("Error while reading page - reading logs aborted. \n");
            return NULL;
        }

        memcpy(logs + (logs_done * LOG_SIZE),
               wholepage + log_addr_in_page(log_id),
               copysize_logs * LOG_SIZE);
        free(wholepage);

/*
        printf("logs: \n");
        for (uint16_t j = 0; j < copysize_logs * LOG_SIZE; j++) {
            if (j % LOG_SIZE == 0) printf(" // \n");
            if (j % PAGE_SIZE_BYTES == 0) printf(" -- // \n");
            printf("0x%.2X ", logs[(logs_done * LOG_SIZE) + j]);
        }
        printf(" -- \n");
  */      
        logs_done += copysize_logs;
        log_id = next_page_log;

    }
    return logs;
    // Returned pointer must be freed by caller.
}

// Write up to n bytes starting from the given log ID. Writes correct log IDs and with the right page offset.
void write_raw_data(uint16_t log_id, uint8_t *raw_data, uint16_t n_bytes) {
    // write_raw_data will assume that, if n_bytes are not enough to cover a page
    // until the end, then the rest of the page is blank.
    // Under the expected use case (ring buf), no point of reading the page first to preserve part of it.
    
    uint16_t n_bytes_formatted = calc_formatted_size(n_bytes);
    uint8_t *neat_data = format_data(log_id, raw_data, n_bytes); // returned VLA is n_bytes_formatted-sized
    uint16_t write_size = 0; 
    for (int32_t n_bytes_left = n_bytes_formatted; n_bytes_left > 0; n_bytes_left -= write_size) {
        uint16_t next_page_log = first_log_in_page(log_id_to_page(log_id) + 1);

        write_size = (next_page_log - log_id) * LOG_SIZE ;
        uint8_t *newpagecontent = calloc(write_size, sizeof(uint8_t));
        memcpy(newpagecontent,
               neat_data + (n_bytes_formatted - n_bytes_left),
               fmin(n_bytes_left, write_size));
        //printf("Data size: %d, logid: 0x%.4X, n_bytes_left %d\n", write_size, log_id, n_bytes_left);
/*
        printf("first log in next page is 0x%.4X. newpagecontent: \n", next_page_log);
        for (uint16_t i = 0; i < write_size; i++) {
            if (i % 8 == 0) printf(" // \n");
            printf("0x%.2X ", newpagecontent[i]);
        }
        printf("\n -- \n");
*/
        uint8_t *current_page = read_page(log_id_to_page(log_id));
        memcpy(current_page + log_addr_in_page(log_id), newpagecontent, write_size);
        write_page(log_id_to_page(log_id), current_page);
        free(current_page);
        free(newpagecontent);
        log_id = next_page_log;
    }
    free(neat_data);
}

// Takes an array of bytes, strips log serials out and returns pure data.
// Takes in the total number of bytes to read. Returns a (SILOG_ZE - sizeof(uint16_t)) * (n_bytes / LOG_SIZE) -sized VLA.
uint8_t * strip_data(uint8_t *log_bytes, uint16_t n_bytes) {
    uint16_t n_logs = n_bytes / LOG_SIZE;
    // Size of pure data is (LOG_SIZE, minus ID size), times the total amount of logs read.

    uint8_t *pure_data = malloc((LOG_SIZE - sizeof(uint16_t)) * n_logs);
    for (uint16_t i = 0; i < n_logs; i ++) {
        memcpy(pure_data + (i * (LOG_SIZE - sizeof(uint16_t))), log_bytes + (i * LOG_SIZE) + sizeof(uint16_t), (LOG_SIZE - sizeof(uint16_t)));
    }
    return pure_data; 
    // Returned pointer must be freed by caller.
}

// Like strip_data, but pruder
// This function should take care of log ID overflow if data will not fit behind the metadata page. 
uint8_t * format_data(uint16_t log_id, uint8_t *raw_bytes, uint16_t n_bytes) {
    uint16_t n_logs = calc_formatted_size(n_bytes) / LOG_SIZE;
    
    /*
    printf("unformatted: \n");
    for (uint8_t i = 0; i < n_bytes + LOG_SIZE; i++) {
        if (i % 8 == 0) printf(" // \n");
        printf("0x%.2X ", raw_bytes[i]);
    }
    printf("\n");
    */

    uint8_t *formatted_data = calloc(n_logs * LOG_SIZE, sizeof(uint8_t));
    
    /*
    printf("allocked: \n");
    for (uint8_t i = 0; i < n_logs * 8 + LOG_SIZE; i++) {
        if (i % 8 == 0) printf(" // \n");
        printf("0x%.2X ", formatted_data[i]);
    }
    */

    //printf("\n -- \n");
    uint16_t log_i = log_id;
    for (uint16_t i = 0; i < n_logs; i ++) {
        memcpy(formatted_data + (i * LOG_SIZE), &(log_i), sizeof(uint16_t));
        log_i++;

        // if the trailing "log" of the raw data is incomplete,
        // then write only as many bytes as it has.
        size_t sz = (size_t) fmin((float) (                LOG_SIZE - sizeof(uint16_t)),
                                  (float) (n_bytes - (i * (LOG_SIZE - sizeof(uint16_t)))));
        memcpy(formatted_data + (i * LOG_SIZE) + 2,
               raw_bytes + (i * (LOG_SIZE - sizeof(uint16_t))),
               sz);

        /*
        printf("creating log: \n");
        for (uint8_t j = 0; j < LOG_SIZE; j++) {
            if (j % 8 == 0) printf(" // \n");
            printf("0x%.2X ", *(formatted_data + (i * LOG_SIZE) + j));
        }
        printf("\n -- \n");
        */
    }

    /*
    printf("formatted (last line is not part of it): \n");
    for (uint8_t i = 0; i < n_logs * 8 + LOG_SIZE; i++) {
        if (i % 8 == 0) printf(" // \n");
        printf("0x%.2X ", formatted_data[i]);
    }
    printf("\n -- \n");
    */

    return formatted_data; 
    // Returned pointer must be freed by caller.
}

meta_t read_meta(void) {
    uint8_t *meta_page = read_page(METADATA_PAGE_ID);
    meta_t meta_contents = {0};
    memcpy(&meta_contents.ver_hash,  meta_page                  + sizeof(uint16_t), sizeof(meta_contents.ver_hash));
    memcpy(&meta_contents.filesize,  meta_page +      LOG_SIZE  + sizeof(uint32_t), sizeof(meta_contents.filesize));
    memcpy(&meta_contents.memsize,   meta_page + (2 * LOG_SIZE) + sizeof(uint32_t), sizeof(meta_contents.memsize));
    memcpy(&meta_contents.freespace, meta_page + (3 * LOG_SIZE) + sizeof(uint32_t), sizeof(meta_contents.freespace));
    memcpy(&meta_contents.cursorpos, meta_page + (4 * LOG_SIZE) + sizeof(uint32_t), sizeof(meta_contents.cursorpos));
    // printf("Cursor pos from read_meta: 0x%.8X\n", meta_contents.cursorpos);
    free(meta_page);
    return meta_contents;
}

void erase_page(uint8_t page_id) {
    // printf("pg %d \n", page_id);
    FILE * fp = mount_at(page_addr_to_byte(page_id), true);;
    if (!fp) {
        printf("Mount failed, unable to erase.\n");
        return;
    }

    uint8_t * zeroes = calloc(PAGE_SIZE_BYTES, sizeof(uint8_t));
    uint32_t count = fwrite(zeroes, sizeof(zeroes[0]), PAGE_SIZE_BYTES, fp);
    if (count != PAGE_SIZE_BYTES) {
        printf("Did NOT successfully erase entire page. \n");
        // Handle this
    }
    free(zeroes);
    unmount(fp);

    uint8_t blankpage[PAGE_SIZE_BYTES] = {0};
    update_meta(page_id, blankpage, false);
}

void erase_flash(void) {
    printf("Writing zeroes to entire memory... \n");
    for (uint16_t i = 0; i < PAGE_COUNT; i++) {
        erase_page(i);
        if (i == PAGE_COUNT / 2) printf("50%% done... \n");
    }
    printf("Flash zeroed successfully. \n");
}

// Erase flash, then write metadata to the metadata page 
void format_flash(void) {
    erase_flash();

    uint8_t meta_page[PAGE_SIZE_BYTES] = {0};
    memcpy(meta_page, (uint8_t[]){METADATA_ID_VERHASH & 0xFF,
                                           ((METADATA_ID_VERHASH >> 8) & 0xFF),
                                           0x0B, 0x16, 0x36, 0xDB, 0x00, 0xB5},
                                           sizeof(uint8_t) * LOG_SIZE) ;    // remove this in prod


    // Log 0x1FE1: Total file size (only updated at format)
    uint32_t filesize = get_file_size();
    memcpy(meta_page + LOG_SIZE, (uint8_t[]){METADATA_ID_FILESIZE & 0xFF, 
                                                      ((METADATA_ID_FILESIZE >> 8) & 0xFF),
                                                      0x00, 0x00,
                                                      filesize & 0xFF,
                                                      (filesize >> 8) & 0xFF,
                                                      (filesize >> 16) & 0xFF,
                                                      (filesize >> 24) & 0xFF},
                                                      sizeof(uint8_t) * LOG_SIZE) ;

    // Log 0x1FE2: Total non-reserved memory size (only updated at format)
    uint32_t memsize = get_memory_size();
    memcpy(meta_page + (2 * LOG_SIZE), (uint8_t[]){METADATA_ID_MEMSIZE & 0xFF, 
                                                      ((METADATA_ID_MEMSIZE >> 8) & 0xFF),
                                                      0x00, 0x00,
                                                      memsize & 0xFF,
                                                      (memsize >> 8) & 0xFF,
                                                      (memsize >> 16) & 0xFF,
                                                      (memsize >> 24) & 0xFF},
                                                      sizeof(uint8_t) * LOG_SIZE) ;

    // Log 0x1FE3: Free space (to user) - always == memory size at format
    uint32_t freespace = memsize;
    memcpy(meta_page + (3 * LOG_SIZE), (uint8_t[]){METADATA_ID_FREESPACE & 0xFF, 
                                                      ((METADATA_ID_FREESPACE >> 8) & 0xFF),
                                                      0x00, 0x00,
                                                      freespace & 0xFF,
                                                      (freespace >> 8) & 0xFF,
                                                      (freespace >> 16) & 0xFF,
                                                      (freespace >> 24) & 0xFF},
                                                      sizeof(uint8_t) * LOG_SIZE) ;

    // Log 0x1FE4: Cursor position (0x0000 at format - nothing to do here except write log ID)
    memcpy(meta_page + (4 * LOG_SIZE), (uint8_t[]){METADATA_ID_CURSOR & 0xFF, 
                                                      ((METADATA_ID_CURSOR >> 8) & 0xFF)},
                                                      sizeof(uint16_t) ) ;

    write_page(METADATA_PAGE_ID, (uint8_t *) meta_page);
    printf("Flash formatted successfully. \n");
}

// Param    page_id: Serial page id (0x00 - 0x255)
//          page_content: Pointer to array containing 32 log_t elements.
//          Array size is checked here.
void write_page(uint8_t page_id, uint8_t page_content[PAGE_SIZE_BYTES]) {
    // Erase it first.
    erase_page(page_id);

    FILE * fp = mount_at(page_addr_to_byte(page_id), true);
    if (!fp) {
        printf("Mount failed, unable to write.\n");
        return;
    }

    uint32_t count = fwrite(page_content, sizeof(log_t), PAGE_SIZE_LOGS, fp);
    if (count != PAGE_SIZE_LOGS) {
        printf("Did NOT successfully erase entire page. \n");
        // Handle this
    }

    unmount(fp);

    update_meta(page_id, page_content, true);
}


// keep in mind that in real flash you need to write page by page.
// This implementation takes a pre-written log, and writes it at the logical address in its ID.
// Later think of how, or if, to overwrite a log in a separate place
uint8_t write_one_log(log_t log) {
    // Confirm that address is valid (within bounds)
    if (log.id >= PAGE_SIZE_LOGS * PAGE_COUNT) {
        printf("Log 0x%.4X is out of bounds (x < 0x%.4X)! \n", log.id, PAGE_SIZE_LOGS * PAGE_COUNT);
        return EXIT_FAILURE;
    }

    // Copy page to RAM
    uint8_t *page_temp = read_page(log_id_to_page(log.id));
    // Modify it and write it back
    memcpy(page_temp + log_addr_in_page(log.id), &log, LOG_SIZE);
    write_page(log_id_to_page(log.id), page_temp);
    free(page_temp);

    return 0;
}

// Writes some data, reads it back, reports if it's as it should be.
// Data cannot already be formatted. strip_data() can be used to get raw from neat.
// Returns 0 on test success, 1 on failure
bool rw_test(uint8_t *raw_data, uint16_t cursor_pos, uint16_t n_bytes) {
    bool err_code = EXIT_FAILURE;

    uint8_t *neat_buf = format_data(cursor_pos, raw_data, n_bytes);
    write_raw_data(cursor_pos, raw_data, n_bytes);

    uint8_t *readback_buf = read_n_logs(cursor_pos, calc_formatted_size(n_bytes) / LOG_SIZE);
    if (memcmp(neat_buf, readback_buf, calc_formatted_size(n_bytes)) == 0) {
        err_code = EXIT_SUCCESS;
    }

    free(neat_buf);
    free(readback_buf);
    return err_code;
}

int main(int argc, char *argv[]) {
    // NOTE free space calculation in testing
    // need to tidy up
    // write documentation
    // besides that I can pretend that it's finished, otherwise I could debug read_n_logs for the 10000th time

    if (argc <= 1) {
        printf("Usage: sffs [command] (option) \n"
                "       [required parameter] (optional parameter) \n"
                "f  Format      Blanks all bits and writes metadata information (Recommended at first use). \n"
                "   Usage:      sffs f \n"
                "\n"
                "e  Erase       Erases n (1 if not specified) pages, backwards starting from cursor position.\n"
                "   Usage:      sffs e (n) \n"
                "\n"
                "r  Read        Reads  n (1 if not specified) logs or pages forwards starting from given ID. \n"
                "   Usage:      sffs r [l|p] [id] (n) \n"
                "\n"
                "w  Write       Writes up to 0xFFFF bytes to flash at the cursor position. Interpreted as string.\n"
                "   Usage:      sffs w [data] \n"
                "\n"
                "m  Meta        Reads out metadata (file and user-accessible memory size, free space, cursor position) \n"
                "   Usage:      sffs m \n"
                "\n"
                "u  Unit test   Performs read/write test \n"
                "   Usage:      sffs u (log position) [data] \n"
                "\n"
                "i  Info        Information about filesystem behavior \n"
                "   Usage:      sffs i \n");
                return 0;
    } else {
        char *cmd = argv[1];
        switch (cmd[0]) {
            case 'r': {     // Read
                // Requires args.
                // r (p|l) [id] [.n]
                if (argc >= 4) {
                    uint16_t n = 1;
                    uint8_t *buf = NULL;
                    uint16_t bufsize = 0;
                    if (argc == 5) {
                        n = (uint16_t) strtol(argv[4], NULL, 10);
                    }
                    
                    char mode = argv[2][0];
                    uint16_t id = (uint16_t) strtol(argv[3], NULL, 10);
                    //printf("mode %c n %d id %d (0x%.4X)\n", mode, n, id, id);
                    if (mode == 'l') {
                        if (n == 1) {
                            buf = read_log(id);
                            bufsize = LOG_SIZE;
                        } else {
                            // fixme: read_n_logs shits itself in the starting log of a page
                            buf = read_n_logs(id, n);
                            bufsize = LOG_SIZE * n;
                        }
                    } else if (mode == 'p') {
                        for (uint16_t i = 0; i < n; i++) {
                            buf = read_page(id);
                            bufsize = PAGE_SIZE_BYTES;
                        }
                    } else {
                    printf("Usage: sffs r [p|l] [id] (number of)\n"
                                  "Example: sffs r p 3 5 - Read 5 pages starting with page 3\n");
                    }

                    // PRINT HERE
                    for (uint16_t i = 0; i < bufsize; i++) {
                        if (i % LOG_SIZE == 0) printf(" // \n");
                        printf("0x%.2X ", buf[i]);
                    }
                    printf(" \n");
                    free(buf);
                } else {
                    printf("Usage: sffs r [p|l] [id] (number of)\n"
                                  "Example: sffs r p 3 5 - Read 5 pages starting with page 3\n");
                }
                break;
            } case 'w': {   // Write raw data to flash. It will tell you the log ID it was stored at and how many logs.
                // w [data]
                if (argc == 3) {
                    uint16_t initialpos = (uint16_t) read_meta().cursorpos;
                    uint32_t formatted_len = calc_formatted_size(strlen(argv[2]));
                    write_raw_data(initialpos, (uint8_t*) argv[2], strlen(argv[2]));
                    printf("Wrote data at log %u, data size was %u logs", initialpos, (uint32_t) ceil( (float) formatted_len / LOG_SIZE));
                } else {
                    printf("Usage: sffs w [data]\n"
                                  "Example: sffs w FWHFA8P - Writes \"FWHFA8P\" starting from the first unused log\n");
                }
                break;
            } case 'e': {   // Erase
                uint16_t cursor_page = log_id_to_page(read_meta().cursorpos);

                if (argc > 2) {
                    uint32_t n = strtol(argv[2], NULL, 10);
                    n = fmin(cursor_page, n); // Do not underflow if there is less than n pages written
                    for (uint32_t i = 0; i < n; i++) {
                        printf("Erasing page 0x%.2X \n", cursor_page);
                        erase_page(cursor_page);
                        cursor_page--;
                    }
                } else {
                    printf("Erasing page 0x%.2X \n", cursor_page);
                    erase_page(cursor_page);
                }
                printf("Done. \n");
                break;
            } case 'f': {   // Format
                format_flash();
                break;
            } case 'm': {   // 
                meta_t meta = read_meta();
                printf("Flash size: %u bytes including 256 reserved \n"
                "Memory accessible to user: %u bytes \n"
                "Cursor position: Log %u (0x%.4X) \n"
                "Free space: %u bytes (%.2f%%) \n",
                 meta.filesize, meta.memsize, meta.cursorpos, meta.cursorpos, meta.freespace, 
                 100 * (meta.freespace / (float) meta.memsize));
                break;
            } case 'u': {
                if (argc >= 3) {
                    uint16_t cursorpos;
                    uint8_t data_pos;
                    if (argc == 4) {
                        data_pos = 3;
                        cursorpos = strtol(argv[2], NULL, 10);
                    } else {
                        data_pos = 2;
                        cursorpos = read_meta().cursorpos;
                    }
                    bool result = rw_test((uint8_t *) argv[data_pos], cursorpos, strlen(argv[data_pos]));
                    printf("Read/write test %s at log %u (0x%.4X) \n", (result ? "failed (uh oh)" : "successful"), cursorpos, cursorpos);
                } else {
                    printf("Usage: sffs u (n) [data] \n"
                    "Example: sffs u 'pretend that this is an easter egg' \n");
                }
                break;
            } case 'i': {
                printf("This is a simulation of a primitive flash memory filesystem. \n"
                               "Instead of physical memory, flash is represented by the file memory.hex. \n"
                               "Memory is made up of 256 pages, each page containing 32 logs, and each log is 8 bytes. \n"
                               "The last page is reserved for metadata. \n"
                               "\n"
                               "The filesystem is meant to be written like a ring buffer. At each write or erase, the \n"
                               "position of the last modification is saved, and the user can only write or erase from \n"
                               "there. At the last non-reserved page, the cursor resets to start, but free space is marked \n"
                               "as 0%% until next format. The cursor only goes forward on write and backward on erase. \n"
                               "The user can read from any position. \n"
                               );
                break;
            } default: {
                printf("Unrecognized argument(s). Run sffs without arguments for usage information. \n");
            }
        }
    }

    /*
    // erase ops
    // erase_flash();
    //erase_page(0);
    //erase_page(1);
    //erase_page(2);
    format_flash();


    // write a page (or many)
    log_t *apage = calloc(PAGE_SIZE_LOGS, sizeof(log_t));
    uint16_t p = 0;
    for (p = 0; p < (PAGE_COUNT * 0.3) ; p++) { // Uncomment if doing this for many pages
        uint16_t l0 = first_log_in_page(p);
        for (uint16_t l = 0; l < PAGE_SIZE_LOGS; l++) {
            log_t temp = {(uint16_t) l0 + l , {'X','X','X','X','X','X'} };    // A nicely serialized blank(?) log
            apage[l] = temp;
        }
        write_page(p, (uint8_t *) apage);
    }
    free(apage);
    char * page_temp = (char *) read_page(0);
    //printf("Wrote page (print truncated at first null): \n%s \n", page_temp);
    free(page_temp);
    */


    // write a log
    //log_t testlog = {byte_addr_to_log(0x20), {'4','5','2','5','2','5'}};
    //write_one_log(testlog);
    //printf("Wrote test log \n");


    /* fill all logs
    printf("Filling logs... \n");
    for (uint16_t l = 0; l < PAGE_SIZE_LOGS * PAGE_COUNT; l++) {
        // printf("Log 0x%.2X \n", l);
        log_t log = {l, {0,1,2,3,4,5}};
        write_one_log(log); // todo write them pagewise

        if (l && l % ((PAGE_SIZE_LOGS * PAGE_COUNT) / 5) == 0) {
            printf("Cleared %f... \n", ((double) l / (double) PAGE_SIZE_LOGS * PAGE_COUNT));  
        }
    }
    */


    /*
    // read a specific log
    uint8_t *temp_log = read_log(0x65);
    printf("Log: \n%s \n", (char*) temp_log);
    free(temp_log);
    */
    /*
    // get memory size (not including reserved) from its location
    meta_t meta_in_ram = read_meta();
    uint32_t freespace = meta_in_ram.freespace; // this might not need to calculate free space, but only read it
    uint32_t memsize   = meta_in_ram.memsize;
    printf("free space: %d out of %d bytes (%.2f%%) \n",
                            freespace,
                            memsize,
                            100 * (freespace / (float) memsize ) );
    */


    /* // Read $somenum logs
    uint8_t somenum = 6;
    uint8_t *somepage = read_n_logs(0x03, somenum);
    uint8_t *somepagepure = strip_data(somepage, somenum * LOG_SIZE);
    
    printf("somepage: \n");
    for (uint8_t i = 0; i < somenum * LOG_SIZE; i++) {
        if (i % LOG_SIZE == 0) printf(" // ");
        printf("0x%.2X ", somepage[i]);
    }
    printf(" \n");
    
    printf("somepagepure: \n");
    for (uint8_t i = 0; i < somenum * (LOG_SIZE - sizeof(uint16_t)); i++) {
        if (i % (LOG_SIZE - sizeof(uint16_t)) == 0) printf(" // ");
        printf("0x%.2X ", somepagepure[i]);
    }
    printf(" \n");
    free(somepage);
    free(somepagepure);
    meta_in_ram = read_meta();
    printf("Cursor pos: 0x%.8X // Free space: 0x%.8X (%d bytes) (%.2f%%) \n", meta_in_ram.cursorpos, meta_in_ram.freespace, meta_in_ram.freespace,
                            100 * (meta_in_ram.freespace / (float) meta_in_ram.memsize ));
    */


    // Write n bytes at cursor
    /*
    meta_t meta_in_ram = read_meta();
    uint32_t nuun_pos = meta_in_ram.cursorpos;
    printf("NUUN Cursor pos: 0x%.8X // Free space: 0x%.8X (%u bytes) (%.3f%%) \n", meta_in_ram.cursorpos, meta_in_ram.freespace, meta_in_ram.freespace,
                            100 * (meta_in_ram.freespace / (float) meta_in_ram.memsize ));

#define nuun 38693  // number of elements (raw)
    uint8_t randomArray[nuun] = {0};
    for (uint16_t i = 0; i < nuun; i++) {
        randomArray[i] = i % UINT8_MAX;
    }

    printf("Calling write raw data at 0x%.8X (0x%.4X) \n", meta_in_ram.cursorpos, (uint16_t) meta_in_ram.cursorpos);
    write_raw_data((uint16_t) meta_in_ram.cursorpos, randomArray, nuun);
    meta_in_ram = read_meta();
    printf("After write: Cursor pos: 0x%.8X // nuun pos: 0x%.8X // Free space: 0x%.8X (%u bytes) (%.3f%%) \n",
                    meta_in_ram.cursorpos,              nuun_pos,         meta_in_ram.freespace, meta_in_ram.freespace,
                                                                          100 * (meta_in_ram.freespace / (float) meta_in_ram.memsize ));
// read it back
    uint8_t *written = read_n_logs(nuun_pos, calc_formatted_size(nuun) / LOG_SIZE);
    printf(" cfs(nuun): %d, // written \n", calc_formatted_size(nuun));
    for (uint16_t i = 0; i < calc_formatted_size(nuun); i++) {
        if (i % LOG_SIZE == 0) printf(" // \n");
        if (i % PAGE_SIZE_BYTES == 0) printf(" -- // \n");
        printf("0x%.2X ", written[i]);
    } *//*
    free(written);
    meta_in_ram = read_meta();   //this must be kept updated manually
    printf("After everything: Cursor pos: 0x%.8X // Free space: 0x%.8X (%u bytes) (%.3f%%) \n", meta_in_ram.cursorpos, meta_in_ram.freespace, meta_in_ram.freespace,
                            100 * (meta_in_ram.freespace / (float) meta_in_ram.memsize ));
    */

    return 0;
}